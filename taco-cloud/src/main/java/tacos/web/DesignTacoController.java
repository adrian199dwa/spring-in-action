package tacos.web;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

import tacos.Taco;
import tacos.Order;
import tacos.Ingredient;
import tacos.Ingredient.Type;
import tacos.data.IngredientRepository;
import tacos.data.TacoRepository;

@Slf4j
@Controller
@RequestMapping("/design")
@SessionAttributes("order")
public class DesignTacoController {

	private final IngredientRepository ingredientRepo;
	private TacoRepository tacoRepo;

	@Autowired
	public DesignTacoController(
			IngredientRepository ingredientRepo,
			TacoRepository tacoRepo) {
		this.ingredientRepo = ingredientRepo;
		this.tacoRepo = tacoRepo;
	}
	
	/*@ModelAttribute
	public void addIngredientsToModel(Model model) {
		List<Ingredient> ingredients = Arrays.asList(
			new Ingredient("FLTO", "przenna", Type.WRAP),
			new Ingredient("COTO", "kukurudziana", Type.WRAP),
			new Ingredient("GRBF", "mielona wołowina", Type.PROTEIN),
			new Ingredient("CARN", "kawałki mięsa", Type.PROTEIN),
			new Ingredient("TMTO", "pomidory w kostę", Type.VEGGIES),
			new Ingredient("LETC", "sałata", Type.VEGGIES),
			new Ingredient("CHED", "cheddar", Type.CHEESE),
			new Ingredient("JACK", "Monterey Jack", Type.CHEESE),
			new Ingredient("SLSA", "sos salsa", Type.SAUCE),
			new Ingredient("SRCR", "śmietana", Type.SAUCE)
		);
		
		Type[] types = Ingredient.Type.values();
		for (Type type : types) {
			model.addAttribute(type.toString().toLowerCase(), 
				filterByType(ingredients, type));
		}
	}*/
	
	@ModelAttribute
	public void addIngredientsToModel(Model model) {
		List<Ingredient> ingredients = new ArrayList<>();
		ingredientRepo.findAll().forEach(i -> ingredients.add(i));
		
		Type[] types = Ingredient.Type.values();
		for (Type type : types) {
			model.addAttribute(type.toString().toLowerCase(), 
				filterByType(ingredients, type));
		}
	}
	
	@GetMapping
	public String showDesignForm(Model model) {
		model.addAttribute("design", new Taco());
		return "design";
	}
	
	private List<Ingredient> filterByType(List<Ingredient> ingredients, Type type) {
	    return ingredients.stream()
		    .filter(x -> x.getType().equals(type))
		    .collect(Collectors.toList());
	}
	
	/*@PostMapping
	public String processDesign(
			@Valid @ModelAttribute("design") Taco design, 
			Errors errors) {
		if (errors.hasErrors()) {
			log.info("I found errors in the design!");
			return "design";
		}
		
		log.info("Przetwarzanie projektu taco: " + design);
		return "redirect:/orders/current";
	}*/
	
	@ModelAttribute(name = "order")
	public Order order() {
		return new Order();
	}
	
	@ModelAttribute(name = "design")
	public Taco taco() {
		return new Taco();
	}
	
	@PostMapping
	public String processDesign(
			@Valid @ModelAttribute("design") Taco design, 
			Errors errors, 
			@ModelAttribute Order order) {
		if (errors.hasErrors()) {
			log.info("I found errors in the design!");
			return "design";
		}
		
		Taco saved = tacoRepo.save(design);
		order.addTaco(saved);
		
		log.info("Przetwarzanie projektu taco: " + design);
		return "redirect:/orders/current";
	}
	
	
}
