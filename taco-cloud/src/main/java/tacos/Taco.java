package tacos;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import java.util.Date;

@Data
public class Taco {
	
	@NotNull
	@Size(min=5, message="Nazwa musi składać sie z przynajmniej pięciu znaków.")
	private String name;
	
	@NotNull
	@Size(min=1, message="Musisz wybrać przynajmniej jeden składnik.")
	//private List<Ingredient> ingredients;
	private List<String> ingredients;
	
	private Long id;
	
	private Date createdAt;
}
