package tacos;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

@Data
public class Order {

	@NotBlank(message="Podanie imienia i nazwiska jest obowiązkowe.")
	private String clientName;
	
	@NotBlank(message="Ulica jest obowiązkowa.")
	private String street;
	
	@NotBlank(message="Miejscowość jest obowiązkowa.")
	private String city;
	
	@NotBlank(message="Województwo jest obowiązkowe.")
	private String state;
	
	@NotBlank(message="Kod jest obowiązkowy.")
	private String zip;
	
	//@CreditCardNumber(message="To nie jest prawidłowy numer karty płatniczej")
	@Digits(integer=18, fraction =0, message="Zły numer karty.")
	private String ccNumber;
	
	@Pattern(regexp="^(0[1-9]|1[0-2])([\\/])([1-9][0-9])$", message="Nie MM/RR.")
	private String ccExpiration;
	
	@Digits(integer=3, fraction=0, message="Nieprawidłowy CVV.")
	private String ccCVV;
	
	private Long id;
	
	private Date placedAt;
	
	private List<Taco> tacos = new ArrayList<>();
  
	public void addTaco(Taco taco) {
		this.tacos.add(taco);
  }
	
}
